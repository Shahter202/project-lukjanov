import "./ProductViews.css";
import imageView1 from "./image-1.png";
import imageView2 from "./image-2.png";
import imageView3 from "./image-3.png";
import imageView4 from "./image-4.png";
import imageView5 from "./image-5.png";

function ProductViews() {
  return (
    <div className="product__header">
      <div className="product__name">Смартфон Apple iPhone 13, синий</div>
      <div className="product__photos">
        <img
          className="product__photo"
          src={imageView1}
          alt="Модель телефона"
        />
        <img
          className="product__photo"
          src={imageView2}
          alt="Модель телефона"
        />
        <img
          className="product__photo"
          src={imageView3}
          alt="Модель телефона"
        />
        <img
          className="product__photo"
          src={imageView4}
          alt="Модель телефона"
        />
        <img
          className="product__photo"
          src={imageView5}
          alt="Модель телефона"
        />
      </div>
    </div>
  );
}

export default ProductViews;
