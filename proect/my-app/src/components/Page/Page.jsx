// import { useState } from "react";
import "./page.css";
import Header from "../Header/Header";
import BreadCrumbs from "../BreadCrumbs/BreadCrumbs";
import Main from "../Main/Main";
import Footer from "../Footer/Footer";
import productData from "../Data";

function Page() {
  // const [ActiveButton, setActiveButton] = useState(
  //   JSON.parse(localStorage.getItem("sidebarButtonChoise"))
  // );
  // localStorage.setItem("sidebarButtonChoise", ActiveButton);
  // const handleClickButton = () => setActiveButton((prevCount) => 1 - prevCount);

  // const [ActiveLike, setActiveLike] = useState(
  //   JSON.parse(localStorage.getItem("sidebarLikeChoise"))
  // );
  // localStorage.setItem("sidebarLikeChoise", ActiveLike);
  // const handleClickLike = () => setActiveLike((prevCount) => 1 - prevCount);

  return (
    <div>
      <Header />
      {/* <Header ActiveButton={ActiveButton} ActiveLike={ActiveLike} /> */}
      <div className="main container">
        <BreadCrumbs list={productData.breadCrumbs} />
        <Main
          // handleClickButton={handleClickButton}
          // ActiveButton={ActiveButton}
          // handleClickLike={handleClickLike}
          // ActiveLike={ActiveLike}
        />
      </div>
      <Footer list={productData.items} />
    </div>
  );
}

export default Page;
