import "./ColorButton.css";
import { memo } from 'react'

function ColorButton(props) {
  const { item, isActive } = props;

  console.log('refresh')

  return (
    <div className={`product__image ${isActive ? "selected" : ""}`}>
      <img
        className="img-button"
        src={item.src}
        alt="Color of product"
        key={item.src}
      />
    </div>
  );
}

export default memo(ColorButton);

