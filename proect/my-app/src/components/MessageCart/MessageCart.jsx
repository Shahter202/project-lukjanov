import "./MessageCart.css";
import { useSelector } from "react-redux";


function MessageCart(props) {
  const countCurt = useSelector((state) => state.cart.products.length);

  return (
    <div className="message-button">
      {/* <div className={`message-cart ${props.clicks && cart.products ? 'hidden' : ''}`}> {" "} Товар удалён</div> */}
      <div className={`message-cart ${props.clicks && countCurt === 1 ? '' : 'hidden'}`}> {" "} Товар добавлен в корзину</div>
    </div>
  );
}

export default MessageCart
