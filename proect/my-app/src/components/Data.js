let productData = {
    breadCrumbs: [
        {
            text: 'Электроника',
            link: '/electronics'
        },
        {
            text: 'Смартфоны и гаджеты',
            link: '/electronics/gadgets'
        },
        {
            text: 'Мобильные телефоны',
            link: '/electronics/gadgets/phone'
        },
        {
            text: 'Apple',
            link: '/electronics/gadgets/phone/apple'
        },
    ],


    items: {
        tel: {
            text: '+7 900 000 0000',
            link: 'tel:+79000000000'
        },

        mail: {
            text: 'partner@mymarket.com',
            link: 'mailto:partner@mymarket.com'
        },
        top: {
            text: 'Наверх',
            link: '#up'
        }
    },

    reviews: [
        {
            id: 1,
            name: 'Марк Г.',
            photo: './photoAuthor/review-1.jpeg',
            rating: './star-5.png',
            periodUse: 'менее месяца',
            plus: `Это мой первый айфон после после огромного количества телефонов на андроиде. 
            Всё плавно, чётко и красиво. довольно шустрое устройство. Камера весьма неплохая, ширик тоже на высоте.`,
            minus: `К самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь,
            а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное.`,
        },
        {
            id: 2,
            name: 'Валерий Коваленко',
            photo: './photoAuthor/review-2.jpeg',
            rating: './star-4.png',
            periodUse: 'менее месяца',
            plus: 'OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго',
            minus: 'Плохая ремонтопригодность',
        }
    
    ]
};

export default productData;
