import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import styles from "./PageIndex.module.css"
import productData from "../Data";
import { Link } from "react-router-dom";

function PageIndex() {
  return (
    <main className={styles.main}>
      <Header />
      <div className={styles.content}>
        <div className={styles.text}>
          Здесь должно быть содержимое главной страницы.
          <br /> Но в рамках курса главная страница используется лишь
          <br /> в демонстрационных целях
        </div>
        <Link to="/product" className={styles.link}>
          Перейти на страницу товара
        </Link>
      </div>
      <Footer list={productData.items} />
    </main>
  );
}

export default PageIndex;
