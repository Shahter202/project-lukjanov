import "./ConfigurationButton.css";


function ConfigurationButton(props) {
  const { item, isActive } = props;
console.log('refresh GGB')
  return (
    <div >
      <button className={`product__button ${isActive ? 'selected' : ''}`}>{item.text}</button>
    </div>
  );
}

export default ConfigurationButton;
