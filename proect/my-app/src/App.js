
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Page from "./components/Page/Page";
import PageIndex from "./components/PageIndex/PageIndex";
import PageNotFound from "./components/PageNotFound/PageNotFound";

function App() {
  return (
    <BrowserRouter>
    <Routes>
    <Route path="/" element={<PageIndex />} />
    <Route path="/product" element={<Page />} />
    <Route path="*" element={<PageNotFound />} />
  </Routes>
  </BrowserRouter>
  );
}

export default App;
