import { createSlice } from "@reduxjs/toolkit";


export const cartSlice = createSlice({
 name: "cart",
 initialState: {
  products: [],
  like: [],
 },

 reducers:
 {
  addProduct: (prevState, action) => {
   const products = action.payload;
   const InCart = prevState.products.some((prevProduct) => prevProduct.id === products.id);

   if (InCart) return prevState;

   return {
    ...prevState,
    products: [...prevState.products, action.payload]

   };
  },

  dellProduct: (prevState, action) => {
   const products = action.payload;

   return {
    ...prevState,
    products: prevState.products.filter((prevProduct) => {
     return prevProduct.id !== products.id;
    })
   };
  },

  addLike: (prevState, action) => {
   const like = action.payload;
   const Inlike = prevState.like.some((prevLike) => prevLike.id === like.id);

   if (Inlike) return prevState;

   return {
    ...prevState,
    like: [...prevState.like, action.payload]

   };
  },

  delllike: (prevState, action) => {
   const like = action.payload;

   return {
    ...prevState,
    like: prevState.like.filter((prevLike) => {
     return prevLike.id !== like.id;
    })
   };
  },
 },
});

export const { addProduct, dellProduct, addLike, delllike, } = cartSlice.actions;
export default cartSlice.reducer;